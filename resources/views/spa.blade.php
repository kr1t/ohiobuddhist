@php $config = [ 'appName' => config('app.name'), 'locale' => $locale = app()->getLocale(),
'locales' => config('app.locales'), 'githubAuth' => config('services.github.client_id'), ]; @endphp
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <title>{{ config('app.name') }}</title>

    <link rel="stylesheet" href="{{ mix('dist/css/app.css') }}" />
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
    />
  </head>
  <link
    href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap"
    rel="stylesheet"
  />
  <link
    href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&family=Taviraj:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;1,100;1,200;1,300;1,400;1,500;1,600&display=swap"
    rel="stylesheet"
  />

  <body>
    <div id="app"></div>

    {{-- Global configuration object --}}
    <script>
      window.config = @json($config);
    </script>

    {{-- Load the application scripts --}}
    <script src="{{ mix('dist/js/app.js') }}"></script>

    <style>
      /* global css */
      body,
      html {
        font-family: 'Roboto', 'Taviraj';
        /* font-family: 'Taviraj', serif; */
      }
    </style>
  </body>
</html>
