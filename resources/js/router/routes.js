function page(path) {
  return () =>
    import( /* webpackChunkName: '' */ `~/pages/${path}`).then(
      m => m.default || m
    );
}

export default [{
    path: "/login",
    name: "login",
    component: page("auth/login.vue")
  },

  {
    path: "/",
    name: "home",
    component: page("home.vue")
  },
  {
    path: "/articles",
    name: "articles",
    component: page("articles.vue")
  },
  {
    path: "/gallery",
    name: "gallery",
    component: page("gallery.vue")
  },
  {
    path: "/events",
    name: "events",
    component: page("events.vue")
  },
  {
    path: "/monks",
    name: "monks",
    component: page("monks.vue")
  },
  {
    path: "/building_projects",
    name: "building_projects",
    component: page("building_projects.vue")
  },
  {
    path: "/about_us",
    name: "about_us",
    component: page("about_us.vue")
  },
  {
    path: "/articles_detail",
    name: "articles_detail",
    component: page("articles_detail.vue")
  },

  {
    path: "/settings",
    component: page("settings/index.vue"),
    children: [{
        path: "",
        redirect: {
          name: "settings.profile"
        }
      },
      {
        path: "profile",
        name: "settings.profile",
        component: page("settings/profile.vue")
      },
      {
        path: "password",
        name: "settings.password",
        component: page("settings/password.vue")
      }
    ]
  },

  {
    path: "*",
    component: page("errors/404.vue")
  }
];
